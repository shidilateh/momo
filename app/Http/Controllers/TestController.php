<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExcelImport;
use Maatwebsite\Excel\Excel;
use PDF;

class TestController extends Controller
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    public function index()
    {
    	$collection = $this->excel->toCollection(new ExcelImport, 'SampleData.xlsx','public');
    	dd($collection[1][1]);
    }

    public function print()
    {
        $collection = $this->excel->toCollection(new ExcelImport, 'list.xlsx','public');
        $collection_new = collect($collection[0]->toArray())->filter(function($val,$index){
            if($index==0)
                return false;

            // dump($val[1]);
            if($val[1]!="")
                return true;
        });

        $data = array_slice($collection_new->toArray(),400);
        // $data = $collection_new->toArray();
        // dd($data);
     
        // dd($collection_new);
        $pdf = PDF::loadView('print',['data'=>$data]);
        // return view('print')->with('data',$data);
        $content = $pdf->download()->getOriginalContent();
        \Storage::put('invoice.pdf',$content) ;
    }
}
