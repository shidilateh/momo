<!DOCTYPE html>
<html>
<head>
    <title></title>
    <!-- <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
    .page-break {
        page-break-after: always;
    }
    .box{
        /*height:1420px;*/
        margin: 0 auto;
        /*border:1px solid #ccc;*/

        /*max-width: 595px;*/
        /*height: 849px;*/
    }
    .box:first-child{

        /*background: url({{asset('aa.png')}});*/
        background-size: 100% 100%;
    }
    .pos1{
        position: absolute;
        top:30px;
        left:40px;
    }
    .pos2{
        position: absolute;
        top:110px;
        left:220px;
    }
    table td.r0{
        height: 500px;
        vertical-align: bottom;
        padding-left: 210px;
    }
    table td.r1{
        height: 54px;
        vertical-align: top;
        padding-left: 210px;
    }
    table td.r2{
        vertical-align: top;
        padding-left: 210px;
    }
    table td.r3{
        vertical-align: top;
        padding-left: 210px;
    }
    table td.r4{
        vertical-align: top;
        padding-left: 210px;
    }
    table td.r5{
        vertical-align: top;
        padding-left: 210px;
    }
    table td.r6{
        vertical-align: top;
        padding-left: 210px;
    }
    table td.r7{
        vertical-align: top;
        padding-left: 210px;
    }
    table td.r8{
        vertical-align: top;
        padding-left: 210px;
    }
    table td.rspace{
        height: 47px;
        vertical-align: top;
        padding-left: 210px;
    }
    table td.r9{
        height: 32px;
        vertical-align: top;
        padding-left: 210px;
    }
    table{
        width:100%;
    }
    table td{
  /*white-space:nowrap;*/
  width:237px;
    }
    .hei{
        height: 31px;
        max-height: 31px !important;
    }
    .bold{
        font-weight: bold;
        font-size: 11px;
    }
    span.alamat {
    width: 500px;
    display: block;
}
    </style>
</head>
<body>
    <!-- <div class="container"> -->
        <!-- <button class="btn-print">asas</button> -->
        <div class="print">
            @foreach($data as $dat)
            <div class="box" style="position: relative">
                @if(!$loop->first)

                    <div class="page-break"></div>
                @endif
                <table>
                    <tbody>
                        <tr>
                            <td class="r0 bold">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="r1 bold">{{$dat[3]}}</td>
                        </tr>
                        <tr>
                            <td class="r2 hei bold">
                                <span class="alamat">{{$dat[5]}}</span></td>
                        </tr>
                        <tr>
                            <td class="r3 hei bold">{{$dat[4]}}</td>
                        </tr>
                        <tr>
                            <td class="r4 hei bold">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="rspace bold">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="r5 hei bold">{{$dat[6]}}</td>
                        </tr>
                        <tr>
                            <td class="r6 hei bold">{{$dat[7]}}</td>
                        </tr>
                        <tr>
                            <td class="r7 hei bold">xtaw</td>
                        </tr>
                        <tr>
                            <td class="r8 hei bold">{{$dat[0]}}</td>
                        </tr>
                        <tr>
                            <td class="r9 bold">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
               <!--  <span class="pos1">{{$dat[3]}}</span>
                <span class="pos2">{{$dat[5]}}</span>
                 -->
            </div>
            @endforeach
        </div>
    <!-- </div> -->
        
</body>
<!-- <script type="text/javascript" src="{{asset('js/app.js')}}"></script> -->
<!-- <script type="text/javascript">
    
$(function(){
    $(document).on('click','.btn-print',function(){
        console.log("as");
        $('.print').printThis({
            debug: false,               // show the iframe for debugging
    importCSS: true,            // import parent page css
    importStyle: true,         // import style tags
    printContainer: true,       // print outer container/$.selector
    loadCSS: "",                // path to additional css file - use an array [] for multiple
    pageTitle: "",              // add title to print page
    removeInline: false,        // remove inline styles from print elements
    removeInlineSelector: "*",  // custom selectors to filter inline styles. removeInline must be true
    printDelay: 333,            // variable print delay
    header: null,               // prefix to html
    footer: null,               // postfix to html
    base: false,                // preserve the BASE tag or accept a string for the URL
    formValues: true,           // preserve input/form values
    canvas: false,              // copy canvas content
    doctypeString: '...',       // enter a different doctype for older markup
    removeScripts: false,       // remove script tags from print content
    copyTagClasses: false,      // copy classes from the html & body tag
    beforePrintEvent: null,     // function for printEvent in iframe
    beforePrint: null,          // function called before iframe is filled
    afterPrint: null  
        });
    })
})
</script> -->
</html>